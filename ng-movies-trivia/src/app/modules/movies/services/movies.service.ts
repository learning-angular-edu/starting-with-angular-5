import {Injectable} from '@angular/core';
import {Movie} from '../models/movie';
import {MOVIES} from '../models/data-movies';

@Injectable({
  providedIn: 'root'
})
export class MoviesService {

  constructor() {
  }

  getMovies(): Promise<Movie[]> {
    return Promise.resolve(MOVIES);
  }
}

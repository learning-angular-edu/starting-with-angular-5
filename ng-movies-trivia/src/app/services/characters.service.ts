import { Injectable } from '@angular/core';
import {Character} from '../models/character';
import {CHARACTERS} from '../models/data-characters';

@Injectable({
  providedIn: 'root'
})
export class CharactersService {

  constructor() { }

  getCharacters(): Promise<Character[]> {
    return Promise.resolve(CHARACTERS);
  }
}
